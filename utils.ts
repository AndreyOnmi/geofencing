import sample from 'lodash/sample';

export interface Polygon {
  latitude: number;
  longitude: number;
}

export interface Marker {
  inside: boolean;
  coordinate: {
    latitude: number;
    longitude: number;
  };
}

/**
 * Extract the polygons from geojson data
 * @param geojson
 */
export function geojsonPolygons(geojson: GeoJSON.FeatureCollection) {
  return geojson.features.map((feature) => {
    let start;
    const polygons = feature.geometry.coordinates[0].map(
      ([longitude, latitude], index) => {
        if (index === 0) {
          start = {
            latitude,
            longitude,
          };
        }
        return {
          latitude,
          longitude,
        };
      },
    );
    // in order for the geofancing the first and last points need to be the same
    return [...polygons, start];
  });
}

export function generatePolygons(
  latitude: number = -84.11012649536133,
  longitude: number = 9.75440030148045,
  factor: number = 0.05,
  total: number = 300,
) {
  const polygons = [];
  for (let index = 0; index < total; index++) {
    let latLB = latitude;
    let lngLB = longitude;
    let latRB = latitude + factor;
    let lngRB = longitude;
    let latRT = latRB;
    let lngRT = lngRB - factor;
    let latLT = latRB - factor;
    let lngLT = lngRT;
    latitude = latRT;
    longitude = lngRT;
    polygons.push([
      {latitude: latLB, longitude: lngLB},
      {latitude: latRB, longitude: lngRB},
      {latitude: latRT, longitude: lngRT},
      {latitude: latLT, longitude: lngLT},
      {latitude: latLB, longitude: lngLB},
    ]);
  }

  return polygons;
}

export function generateMarkers(
  polygons: Array<Array<Polygon>>,
  factor: number = 0.05,
) {
  return polygons.map((polygon) => {
    const coordinate = {...sample(polygon)};
    if (sample([true, false])) coordinate.latitude += Math.random() * factor;
    if (sample([true, false])) coordinate.longitude += Math.random() * factor;
    return {
      inside: false,
      coordinate,
    };
  });
}

export default generatePolygons;
