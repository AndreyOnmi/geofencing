# Installation

## Repo

<https://bitbucket.org/AndreyOnmi/geofencing/src/master/>

## react-native-geo-fencing <https://github.com/surialabs/react-native-geo-fencing#readme>

`$ npm install --save react-native-geo-fencing`

## react-native-maps <https://github.com/react-native-community/react-native-maps>

npm install react-native-maps --save-exact

# Testing

## Generate GEOJSON

to generate Geojson <http://geojson.io/#map=16/9.9980/-84.1204>

generated GEOJSON used is on ./data.json

## Utils

On utils there are function to generate polygons and markers

## Ideas for improvements

1. Load only geofancing for the current location zone
2. check location on server, passing the lat/lng
