import React, {SyntheticEvent} from 'react';
import {StyleSheet, Dimensions, View, StatusBar} from 'react-native';
import MapView, {Marker, Polygon} from 'react-native-maps';
import GeoFencing from 'react-native-geo-fencing';
import Snackbar from 'react-native-snackbar';
import * as bluebird from 'bluebird';
import get from 'lodash/get';
import sampleSize from 'lodash/sampleSize';

import {
  geojsonPolygons,
  generatePolygons,
  generateMarkers,
  Marker as IMarker,
} from '../utils';

const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.08;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const REGION = {
  latitude: 9.998436,
  longitude: -84.117204,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

class Map extends React.Component {
  constructor(props) {
    super(props);
    const marker = {
      inside: false,
      coordinate: {latitude: REGION.latitude, longitude: REGION.longitude},
    };
    const polygons = props.geojson
      ? geojsonPolygons(props.geojson)
      : generatePolygons(REGION.latitude, REGION.longitude, 0.025);

    this.state = {
      marker,
      polygons,
      markers: generateMarkers(sampleSize(polygons, 100), 0.025),
      points: polygons.map((polygon) =>
        polygon.map(({latitude, longitude}) => ({
          lat: latitude,
          lng: longitude,
        })),
      ),
    };

    this.geofance(this.state.marker)
      .then((marker) => this.setState({marker}))
      .catch((err) => console.error('not inside', err));

    console.log('total markers', this.state.markers.length);
    // this breaks the app because
    // request limit, this could be implemented on backend sending the lat/lng of each marker
    /* Promise.all(this.state.markers.map((marker) => this.geofance(marker)))
      .then((markers) => this.setState({markers}))
      .catch((err) => console.error('validate markers', err)); */
  }

  /**
   * Check if the marker is inside a polygon
   */
  geofance = async (marker: IMarker) => {
    const location = {
      lat: marker.coordinate.latitude,
      lng: marker.coordinate.longitude,
    };
    let inside = false;

    try {
      inside = await bluebird.any(
        this.state.points.map((points) =>
          GeoFencing.containsLocation(location, points),
        ),
      );
    } catch (err) {
      console.log('geofance error: not inside valid area');
    }
    return {...marker, inside};
  };

  handleMarketChange = async (event: SyntheticEvent) => {
    let marker = {
      inside: false,
      coordinate: event.nativeEvent.coordinate,
    };
    try {
      marker = await this.geofance(marker);
    } catch (err) {
      console.error('handleMarketChange error:', err);
    }
    Snackbar.show({
      text: marker.inside ? 'Valid Location' : 'Invalid Location',
      duration: Snackbar.LENGTH_SHORT,
    });
    this.setState({marker});
  };

  handleMarkerDrag = (event: SyntheticEvent) => {
    const coordinate = get(
      event,
      'nativeEvent.coordinate',
      this.marker.coordinate,
    );
    this.setState({
      marker: {
        inside: false,
        coordinate,
      },
    });
  };

  render() {
    return (
      <View style={StyleSheet.absoluteFillObject}>
        <StatusBar />
        <MapView
          style={styles.map}
          initialRegion={REGION}
          showsUserLocation={true}
          followUserLocation={true}
          zoomEnabled={true}
          showsScale={true}
          onPress={this.handleMarketChange}>
          <Marker
            draggable
            coordinate={this.state.marker.coordinate}
            key={this.state.marker.inside ? 'inside' : 'outside'}
            pinColor={this.state.marker.inside ? 'blue' : 'red'}
            onDragEnd={this.handleMarkerDrag}
          />

          {/* 
            Render plygons/geofances areas
            just for testing, not needed to validate a location
           */}
          {this.state.polygons.map((coors, index) => (
            <Polygon
              key={index}
              coordinates={coors}
              strokeColor="#F00"
              fillColor="rgba(255,0,0,0.5)"
              strokeWidth={1}
            />
          ))}

          {/* Render markers */}
          {this.state.markers.map((marker, index) => (
            <Marker
              draggable
              coordinate={marker.coordinate}
              key={marker.inside ? `inside-${index}` : `outside-${index}`}
              pinColor={marker.inside ? 'blue' : 'red'}
            />
          ))}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    height: '100%',
    // marginVertical: 50,
  },
});

export default Map;
